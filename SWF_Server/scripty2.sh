#!/bin/bash
link=$(head --lines 1 $uniqueID.txt) #get the link provided by the phone

songy="$uniqueID.mp4"
echo "songy is $songy"
printf "$songy\nno \n" > $uniqueID.txt  #the client periodically checks this file to see if the file is ready to be downloaded

echo "SCRIPTY2 is executing youtube-dl $link and songy is $songy and uniqueID is $uniqueID"
cd testing2/
./youtube-dl -o $uniqueID $link
ffmpeg -i $uniqueID $songy
cd ../
printf "$songy\nyes\n$link" > $uniqueID.txt     #tell the client the file is ready to download
cd testing2/
rm $uniqueID    #remove raw video file
wget -O testing2/$uniqueID.txt $link
awk '/<\/title>/ && b {print s; exit} /<title>/{s="";next}/<*>/{b=1}{s=s?s RS $0:$0}' $uniqueID.txt >> list.txt
rm $uniqueID.txt