#!/bin/bash
find *.mp3 -type f -cmin +20 | xargs /bin/rm -rfv
find *.mp4 -type f -cmin +30 | xargs /bin/rm -rfv
find *.part -type f -cmin +7 | xargs /bin/rm -rfv >> logmaintainance.log
cd ../
find *.txt -type f -cmin +20 | xargs /bin/rm -rfv
